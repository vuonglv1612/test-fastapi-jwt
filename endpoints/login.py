from fastapi import APIRouter, HTTPException
from datetime import datetime, timedelta
from pydantic import BaseModel
from constants import SECRET_KEY
from utils import load_db
import jwt

router = APIRouter()
fake_database = load_db()


class UserLogin(BaseModel):
    username: str
    password: str


class LoginResponse(BaseModel):
    access_token: str
    username: str
    expire_at: datetime
    created_at: datetime


@router.post("/", response_model=LoginResponse)
async def login(user: UserLogin):
    user_in_db = fake_database.get(user.username)
    if not user_in_db or user_in_db.get("password") != user.password:
        raise HTTPException(status_code=401, detail="Username or password is incorrect!")
    created_at = datetime.utcnow()
    expire_at = datetime.utcnow() + timedelta(hours=1)
    payload = {
        "username": user.username,
        "exp": expire_at,
        "iat": created_at
    }
    access_token = jwt.encode(payload, key=SECRET_KEY, algorithm="HS256")

    response = LoginResponse(access_token=access_token, expire_at=expire_at, created_at=created_at,
                             username=user.username)
    return response
